from helper import get_two_points_distance

# POINTS
#
#  0 P Oko          L Oko 1
#
#
#           2 Nos
#
#    3 P Kącik  L Kącik 4
#
#          5 Broda


def get_ratios(points):
    right_ratio = get_right_ratio(points)
    left_ratio = get_left_ratio(points)

    return right_ratio, left_ratio


def get_right_ratio(points):
    nose_to_right_eye = get_two_points_distance(points[2], points[0])
    nose_to_right_corner = get_two_points_distance(points[2], points[3])
    right_corner_to_eye = get_two_points_distance(points[3], points[0])
    return (nose_to_right_corner + nose_to_right_eye) / right_corner_to_eye


def get_left_ratio(points):
    nose_to_left_eye = get_two_points_distance(points[2], points[1])
    nose_to_left_corner = get_two_points_distance(points[2], points[4])
    left_corner_to_eye = get_two_points_distance(points[1], points[4])
    return (nose_to_left_corner + nose_to_left_eye) / left_corner_to_eye
