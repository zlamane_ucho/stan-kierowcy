import numpy as np
import dlib
from numpy import linalg as la
from imutils import face_utils
from playsound import playsound
from threading import Thread

ALARM_PATH = "./files/alarm.wav"
SHAPE_PREDICTOR_PATH = './files/shape_predictor_68_face_landmarks.dat'
LEFT_EYE_LANDMARKS = list(range(42, 48))
RIGHT_EYE_LANDMARKS = list(range(36, 42))
ORIENTATION_POINTS = [36, 45, 33, 48, 54, 8]
PREDICTOR = dlib.shape_predictor(SHAPE_PREDICTOR_PATH)


def get_eyes_landmarks(img, face):
    face_landmarks_np = face_utils.shape_to_np(PREDICTOR(img, face))
    left_eye = face_landmarks_np[LEFT_EYE_LANDMARKS]
    right_eye = face_landmarks_np[RIGHT_EYE_LANDMARKS]

    return left_eye, right_eye


def get_orientation_points(img, face):
    face_landmarks_np = face_utils.shape_to_np(PREDICTOR(img, face))
    return face_landmarks_np[ORIENTATION_POINTS]


def get_two_points_distance(u, v):
    u_v = u - v
    return la.norm(u_v, ord=2)


def get_ear(eye):
    ver_dist_1 = get_two_points_distance(eye[1], eye[5])
    ver_dist_2 = get_two_points_distance(eye[2], eye[4])
    hor_dist = get_two_points_distance(eye[0], eye[3])

    return (ver_dist_1 + ver_dist_2) / (hor_dist * 2.0)


def get_avg_ear(eyes):
    return (get_ear(eyes[0]) + get_ear(eyes[1])) / 2


def play_alarm():
    PLAY_ALARM_THREAD = Thread(target=playsound, args=[ALARM_PATH])
    PLAY_ALARM_THREAD.start()

    return PLAY_ALARM_THREAD
