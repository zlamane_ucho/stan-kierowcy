import cv2

RED = (0, 0, 255)
GREEN = (0, 255, 0)
BLUE = (255, 0, 0)
FONT = cv2.FONT_HERSHEY_SIMPLEX


def draw_eyes(img, eyes):
    cv2.drawContours(img, [eyes[0]], -1, BLUE, 1)
    cv2.drawContours(img, [eyes[1]], -1, RED, 1)


def draw_point(img, point):
    cv2.circle(img, (point[0], point[1]), 6, BLUE, -1)


def print_text(img, text, position):
    cv2.putText(img, text, position, FONT, 1, RED, 3)


def print_face_direction(img, right_ratio, left_ratio):
    if right_ratio < 1.15:
        print_text(img, "RIGHT", (100, 30))
    if left_ratio < 1.15:
        print_text(img, "LEFT", (100, 30))
