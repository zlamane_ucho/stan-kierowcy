import cv2
import dlib
import numpy as np
from opencv import draw_eyes, print_text, draw_point, print_face_direction
from helper import get_eyes_landmarks, get_avg_ear, play_alarm, get_orientation_points
from orientation import get_ratios
from threading import Thread

FACE_DETECTOR = dlib.get_frontal_face_detector()

THRESHOLD = 0.25
FRAMES_TO_ALARM_AMOUNT = 20

SLEEPY_FRAMES_AMOUNT = 0
PLAY_ALARM_THREAD = Thread()

camera = cv2.VideoCapture(0)

while True:
    ret, frame = camera.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    detected_faces = FACE_DETECTOR(gray)

    for face in detected_faces:

        # EAR
        eyes = get_eyes_landmarks(gray, face)
        draw_eyes(frame, eyes)
        ear = get_avg_ear(eyes)
        print_text(frame, "{:.2f}".format(ear), (20, 30))

        # DIRECTION
        points = get_orientation_points(gray, face)
        print_face_direction(frame, *get_ratios(points))

        for point in points:
            draw_point(frame, point)

        is_sleepy = ear < THRESHOLD

        if is_sleepy:
            SLEEPY_FRAMES_AMOUNT += 1
            print_text(frame, "Sleepy...", (20, 70))
            print_text(frame, str(SLEEPY_FRAMES_AMOUNT), (20, 110))

            is_sleepy_enough_time = SLEEPY_FRAMES_AMOUNT > FRAMES_TO_ALARM_AMOUNT
            is_alarm_turned_off = not PLAY_ALARM_THREAD.is_alive()

            should_alarm_ring = is_sleepy_enough_time & is_alarm_turned_off

            if should_alarm_ring:
                print_text(frame, "Ring ring...", (20, 150))
                PLAY_ALARM_THREAD = play_alarm()
        else:
            SLEEPY_FRAMES_AMOUNT = 0

    cv2.imshow('Camera', frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break


camera.release()
cv2.destroyAllWindows()
